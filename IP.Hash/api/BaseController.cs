using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace IP.Hash
{
    public class BaseController : ApiController
    {
        /// <summary>
        /// 淘宝
        /// </summary>
        public static string Taobao
        {
            get
            {
                return ConfigurationManager.AppSettings["Taobao"];
            }
        }
        /// <summary>
        /// IP定向
        /// </summary>
        public static string PcOnline
        {
            get
            {
                return ConfigurationManager.AppSettings["PcOnline"];
            }
        }
    }
}

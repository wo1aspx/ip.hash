using Microsoft.Owin.Hosting;
using System;
using System.Configuration;
using Topshelf;

namespace IP.Hash
{
    class Program
    {
        /// <summary>
        /// self访问地址
        /// </summary>
        private static string Adrs
        {
            get
            {
                return ConfigurationManager.AppSettings["address"];
            }
        }
        /// <summary>
        /// 服务名称
        /// </summary>
        private static string ServerName
        {
            get
            {
                return ConfigurationManager.AppSettings["ServerName"];
            }
        }
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<Application>(s =>
                {
                    s.ConstructUsing(name => new Application());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem().StartAutomatically();
                x.SetDescription("IP.Hash IP自动识别服务启动");
                x.SetDisplayName(ServerName);
                x.SetServiceName(ServerName);
            });
        }
        private class Application
        {
            private IDisposable _host;
            public void Start()
            {
                Console.WriteLine("Game.IP.Hash Server starting.");
                Console.WriteLine();
                _host = WebApp.Start<Startup>(Adrs);
                Console.WriteLine("Game.IP.Hash Server started.");
                Console.WriteLine();
            }
            public void Stop()
            {
                _host.Dispose();
            }
        }
    }
}
using Beginor.Owin.StaticFile;
using Microsoft.Owin;
using Owin;
using System.Collections.Generic;
using System.Web.Http;

[assembly: OwinStartup(typeof(IP.Hash.Startup))]

namespace IP.Hash
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{action}/{id}",
                new
                {
                    id = RouteParameter.Optional
                });
            app.UseWebApi(config);
            app.Map("/file", map =>
            {
                map.UseStaticFile(new StaticFileMiddlewareOptions
                {
                    RootDirectory = @".\assets",
                    EnableETag = true,
                    EnableHtml5LocationMode = true,
                    MimeTypeProvider = new MimeTypeProvider(new Dictionary<string, string>
                    {
                        { ".html", "text/html" },
                        { ".htm", "text/html" },
                        { ".dtd", "text/xml" },
                        { ".xml", "text/xml" },
                        { ".ico", "image/x-icon" },
                        { ".css", "text/css" },
                        { ".js", "application/javascript" },
                        { ".json", "application/json" },
                        { ".jpg", "image/jpeg" },
                        { ".png", "image/png" },
                        { ".gif", "image/gif" },
                        { ".config", "text/xml" },
                        { ".woff2", "application/font-woff2"},
                        { ".eot", "application/vnd.ms-fontobject" },
                        { ".svg", "image/svg+xml" },
                        { ".woff", "font/x-woff" },
                        { ".txt", "text/plain" },
                        { ".log", "text/plain" },
                        { ".TL", "application/x-zip-compressed" },
                        { ".fnt", "application/octet-stream" },
                        { ".apk", "application/vnd.android" }
                    })
                });
            });
        }
    }
}



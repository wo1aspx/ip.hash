using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IP.Hash
{
    public class pconline
    {
        public string ip { get; set; }
        public string pro { get; set; }
        public string proCode { get; set; }
        public string city { get; set; }
        public string cityCode { get; set; }
        public string region { get; set; }
        public string regionCode { get; set; }
        public string addr { get; set; }
        public string regionNames { get; set; }
        public string err { get; set; }
    }
}
